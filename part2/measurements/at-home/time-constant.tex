\subsubsection{Time constant}
\FloatBarrier

The reader is expected to be familiar with the concept of time constants. If
you are not I would recommend
\href{https://www.electronics-tutorials.ws/rc/rc\_1.html}{this tutorial}.
This is not an RC circuit, but the same theory applies.

\begin{align}
    & \Delta y = y_h - y_l  \label{eqn:tau-Delta-y} \\
    & y = y_l+\Delta y\cdot\left(1-e^{-t/\tau}\right)  \label{eqn:tau-rise} \\
    & y = y_h-\Delta y\cdot\left(1-e^{-t/\tau}\right)  \label{eqn:tau-fall}
\end{align}

Equation \ref{eqn:tau-rise} is the rising edge and equation \ref{eqn:tau-fall}
is the falling edge where \(y\) is the instantaneous signal level, \(\Delta y\)
is the difference between the steady state low (\(y_l\)) and high (\(y_h\))
levels, \(t\) is the time and \(\tau\) is the time constant.

The exact steady state levels are not possible to determine due to the
AC-coupling on the audio input and noise on the low level. The low point
has been chosen quite early and may be inaccurate, see figure
\ref{fig:tau-fall}.

Normally the time constant is taken as the time between the signal starts
diverging and the signal being at 63.2\ \% (\(1-e^{-1}\)), this is the point
where \(t = \tau\). \(\tau\) should be identical independent of where it is measured.  The time constant for this circuit has been measured over a larger
range to see if something strange may be going on:
\begin{itemize}
    \item[\(\tau_1\)] Between the start and \(1-e^{-1}\) (63.2\ \%)
    \item[\(\tau_2\)] Between \(1-e^{-1}\) and \(1-e^{-2}\) (86.5\ \%)
    \item[\(\tau_3\)] Between \(1-e^{-2}\) and \(1-e^{-3}\) (95.0\ \%)
    \item[\(\tau_4\)] Between \(1-e^{-3}\) and \(1-e^{-4}\) (98.2\ \%)
\end{itemize}
See figure \ref{fig:tau-rise} or \ref{fig:tau-fall} for an example.

\textbf{The test was performed} in the dark with an LED fed with a
low-frequency (5\ Hz) square wave with sharp edges.  The signal measured
is the current output through a 9.4\ \(\Omega\) resistor.  The voltage
levels are unknown.

\begin{figure}[h]
    \centering
    \includegraphics[width=15cm]{images/time-constant/signal.png} \\
    Some unexpected switching? artifacts are circled in black. Some bumps
    on the lower half, presumably caused by mains hum, are circled in red.
    The first red point is being used to get \(y_l\) for the falling edge.
    \caption{Measured signal of 5Hz flashes}
    \label{fig:tau-signal}
\end{figure}

The test was repeated a few times for both rising and falling edge. The results
can be seen in table \ref{tab:tau-rise} and table \ref{tab:tau-fall}.

\begin{table}[h]
    \centering
    \(\tau\) in milliseconds \\
    \begin{tabular}{|l|rrrr|r|}
        \hline
        Sample&\(\tau_1\)&\(\tau_2\)&\(\tau_3\)&\(\tau_4\)&\(\tau_{avg}\) \\
        \hline
        1      &3.87   &6.49   &6.96   &5.10   &5.605 \\
        2      &3.89   &6.70   &6.75   &5.50   &5.710 \\
        3      &3.79   &6.59   &6.70   &5.87   &5.738 \\
        4      &3.74   &6.54   &7.06   &5.55   &5.723 \\
        5      &3.79   &6.59   &7.06   &5.14   &5.645 \\
        \hline
        avg    &3.816  &6.582  &6.906  &5.432  &5.684 \\
       %stddev &0.0557 &0.0697 &0.1531 &0.2849 &0.0506 \\
        stddev &0.056  &0.070  &0.153  &0.285  &0.051 \\
        \hline
    \end{tabular} \\
    Total: average = 5.684 ms, standard deviation = 1.221 ms
    \caption{Rising time constant}
    \label{tab:tau-rise}
\end{table}

\begin{table}[h]
    \centering
    \(\tau\) in milliseconds \\
    \begin{tabular}{|l|rrrr|r|}
        \hline
        Sample&\(\tau_1\)&\(\tau_2\)&\(\tau_3\)&\(\tau_4\)&\(\tau_{avg}\) \\
        \hline
        1      &4.00   &4.88   &3.64   &2.55   &3.768 \\
        2      &4.00   &4.93   &3.63   &2.44   &3.750 \\
        3      &3.84   &4.41   &3.43   &2.44   &3.530 \\
        4      &3.58   &3.63   &2.70   &1.61   &2.880 \\
       %5       4.00    6.90    6.07    3.06    5.008       BAD
        \hline
        avg    &3.855  &4.462  &3.350  &2.260  &3.482 \\
        stddev &0.172  &0.522  &0.385  &0.378  &0.360 \\
        \hline
    \end{tabular} \\
    Total: average = 3.482 ms, standard deviation = 0.895 ms
    \caption{Falling timeconstant}
    \label{tab:tau-fall}
\end{table}

\FloatBarrier

As can be seen in the tables, \(\tau\) does not seem to be constant.
Potential sources of distortion:
\begin{itemize}
    \item Noise pick-up (mains hum)
    \item The high-pass action of the audio input
    \item Unkown properties of the LDR or circuit
    \item Capacitance on the GPIO pin
\end{itemize}

\FloatBarrier

\begin{figure}[h]
    \centering
    \includegraphics[width=15cm]{images/time-constant/rise-time/risetime2.png}
    \caption{Measuring \(\tau\) on rising edge}
    \label{fig:tau-rise}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=15cm]{images/time-constant/fall-time-1/falltime2.png}
    \caption{Measuring \(\tau\) on falling edge}
    \label{fig:tau-fall}
\end{figure}

\FloatBarrier
