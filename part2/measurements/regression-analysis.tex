\subsection{Regression analysis}
\label{regression-analysis}

%  [cat]-1k-illum
%  day2-100k-illum-24V

Source code of the program is in the section ``\nameref{regression-program}''.

\subsubsection{Repeated theory}
$$ R \propto E^{-\gamma} $$
$R$ is the resistance, $E$ is the illuminance and $\gamma$ (gamma) is a
``constant'' equivalent to the sensitivity of the LDR.

Typical values appear to be around 0.5 which means that the conductance of
an LDR is approximately proportional to the square root of the illuminance.

Gamma is usually defined in the datasheet as
$$ \log\left(\frac{R_{10}}{R_{100}}\right) $$
where $R_{10}$ is the resistance at 10 lux and $R_{100}$ is the resistance at
100 lux.

The light sensor made in the project converts the conductance of the LDR
linearly to a voltage, hence:
$$ U \propto E^\gamma $$

On a logarithmic scale, gamma will be the slope of the curve and all the gamma
plots below are made by taking the derivative function of the model an plotting
in a logarithmic scale.


\subsubsection{Data}
\begin{itemize}
    \item 1k range measured with multimeter and phone
    \item 1k range measured with AD4ETH and HD2102
    \item 100k range measured with multimeter and phone
    \item 100k range measured with AD4ETH and HD2102
\end{itemize}
Some models will use the logarithms of the voltage and illuminance as input
data.


\subsubsection{Models}
I have been unable to find a more complex model of the resistance of an LDR,
but I suspect $\gamma$ is not constant over a larger range.

The data will be fitted to six different models:
\setcounter{equation}{0}
\begin{align}
    & U = k_0 \cdot E^{k_1} + U_{offset} \\
    & U = k_0 \cdot E^{k_1} \\
    & \log_{10}(U) = k_1\cdot \log_{10}(E) + k_0 \\
    & \log_{10}(U) = k_2\cdot \log_{10}(E)^2 + k_1\cdot \log_{10}(E) + k_0 \\
    & \log_{10}(U) = k_3\cdot \log_{10}(E)^3 + k_2\cdot \log_{10}(E)^2
        + k_1\cdot \log_{10}(E) + k_0 \\
    & \log_{10}(U) = k_4\cdot \log_{10}(E)^4 + k_3\cdot \log_{10}(E)^3
        + k_2\cdot \log_{10}(E)^2 + k_1\cdot \log_{10}(E) + k_0
\end{align}
where $U$ is the voltage in volts and $E$ the illuminance in lux.

Logarithms are performed before least squares fit for equations three to six.

The offset voltage is hard coded to 0.1298 V for the 1k range and 0.0157 V for
the 100k range.  These values are measured with the multimeter and may be off
for data from the AD4ETH.

\subsubsection{Results}

\begin{tabular}{|l|l|}
    \hline
    Line & Equation \\
    \hline
    Solid black  & 1 \\
    Solid red    & 2 \\
    Solid blue   & 3 \\
    Dashed black & 4 \\
    Dashed red   & 5 \\
    Dashed blue  & 6 \\
    \hline
\end{tabular}

$\gamma$ is observed to be variable, lower at higher illuminance levels.

The observed gamma value is different between tests made with the phone and
with the HD2102 hinting that the light sensor in the phone has a noticeable
non-linearity.

\FloatBarrier

\begin{figure}[h]
    \centering
    \textbf{Phone -- 1k} \\
    \includegraphics[width=8cm]{images/regression/phone-1k-lin.eps}
    \includegraphics[width=8cm]{images/regression/phone-1k-log.eps} \\
    \includegraphics[width=8cm]{images/regression/phone-1k-gamma.eps} \\
    Linear-linear illuminance to voltage, logarithmic-logarithmic illuminance
    to voltage, \\
    $\gamma$ (sensitivity) as a function of logarithmic illuminance \\
    \begin{tabular}{|l|l|l|l|}
        \hline
        Model & $r^2$ & $\gamma$ at 10 and 100 Lux & Observations \\
        \hline
        1 (Solid black) & 0.9775    & (0.5431)      & Bad fit on log plot \\
        2 (Solid red)  & 0.9768    & 0.5268        & Acceptable \\
        3 (Solid blue) & 0.9406    & 0.5024        & Acceptable \\
        4 (Dashed black) & 0.9412    & 0.540/0.504   & Acceptable \\
        5 (Dashed red)  & 0.9534    & 0.620/0.393   & Bad fit on lin plot,
                                              doesn't extrapolate \\
        6 (Dashed blue) & 0.9572    & 0.543/0.432   & Doesn't extrapolate \\
        \hline
    \end{tabular}
    \par
    \textbf{Parameters:} \\
    \begin{tabular}{|l|r|r|r|r|r|}
        \hline
        Model   & $k_0$   & $k_1$  & $k_2$   & $k_3$  & $k_4$
                                                        or $U_{offset}$ \\
        \hline
        1       &0.237766 &0.543088&         &        &0.129800 \\
        2       &0.268535 &0.526831&         &        &         \\
        \hline
        3       &-0.506641&0.502405&         &        &         \\
        4       &-0.568727&0.575993&-0.018668&        &         \\
        5       &-0.972417&1.513753&-0.613678&0.111182&         \\
        6       &-1.412182&3.093947&-2.284064&0.792977&-0.095120\\
        \hline
    \end{tabular}
    \caption{Regression analysis for 1k range with phone}
\end{figure}

\begin{figure}[h]
    \centering
    \textbf{HD2102 -- 1k} \\
    \includegraphics[width=8cm]{images/regression/ad4eth-1k-lin.eps}
    \includegraphics[width=8cm]{images/regression/ad4eth-1k-log.eps} \\
    \includegraphics[width=8cm]{images/regression/ad4eth-1k-gamma.eps} \\
    Linear-linear illuminance to voltage, logarithmic-logarithmic illuminance
    to voltage, \\
    $\gamma$ (sensitivity) as a function of logarithmic illuminance \\
    \begin{tabular}{|l|l|l|l|}
        \hline
        Model & $r^2$ & $\gamma$ at 10 and 100 Lux & Observations \\
        \hline
        1 (Solid black)  & 0.9940    & (0.6556)      & Bad fit on log plot \\
        2 (Solid red)    & 0.9943    & 0.6388        & Acceptable \\
        3 (Solid blue)   & 0.9967    & 0.6747        & Acceptable \\
        4 (Dashed black) & 0.9982    & 0.727/0.665   & Acceptable \\
        5 (Dashed red)   & 0.9984    & 0.734/0.644   & Acceptable \\
        6 (Dashed blue)  & 0.9984    & 0.729/0.693   & Acceptable \\
        \hline
    \end{tabular}
    \par
    \textbf{Parameters:} \\
    \begin{tabular}{|l|r|r|r|r|r|}
        \hline
        Model   & $k_0$   & $k_1$  & $k_2$   & $k_3$  & $k_4$
                                                        or $U_{offset}$ \\
        \hline
        1       &0.108977 &0.655589&         &        &0.129800 \\
        2       &0.123673 &0.638774&         &        &         \\
        \hline
        3       &-1.001327&0.674670&         &        &         \\
        4       &-1.085690&0.789041&-0.031058&        &         \\
        5       &-1.132386&0.924567&-0.120805&0.016901&         \\
        6       &-1.142212&0.967297&-0.169340&0.037457&-0.002932\\
        \hline
    \end{tabular}
    \caption{Regression analysis for 1k range with HD2102}
    \label{fig:regression-result}
\end{figure}

\begin{figure}[h]
    \centering
    \textbf{Phone -- 100k} \\
    \includegraphics[width=8cm]{images/regression/phone-100k-lin.eps}
    \includegraphics[width=8cm]{images/regression/phone-100k-log.eps} \\
    \includegraphics[width=8cm]{images/regression/phone-100k-gamma.eps} \\
    Linear-linear illuminance to voltage, logarithmic-logarithmic illuminance
    to voltage, \\
    $\gamma$ (sensitivity) as a function of logarithmic illuminance \\
    \begin{tabular}{|l|l|l|l|}
        \hline
        Model & $r^2$ & $\gamma$ at 10 and 100 Lux & Observations \\
        \hline
        1 (Solid black)  & 0.9755    & (0.4470)      & Bad fit on log plot \\
        2 (Solid red)    & 0.9757    & 0.4453        & Bad fit on log plot \\
        3 (Solid blue)   & 0.9820    & 0.5737        & Bad fit on lin plot \\
        4 (Dashed black) & 0.9905    & 0.725/0.644   & Acceptable \\
        5 (Dashed red)   & 0.9910    & 0.678/0.658   & Acceptable \\
        6 (Dashed blue)  & 0.9912    & 0.671/0.683   & Gamma curve seems off \\
        \hline
    \end{tabular}
    \par
    \textbf{Parameters:} \\
    \begin{tabular}{|l|r|r|r|r|r|}
        \hline
        Model   & $k_0$   & $k_1$  & $k_2$   & $k_3$  & $k_4$
                                                        or $U_{offset}$ \\
        \hline
        1       &0.066916 &0.446975&         &        &0.015700 \\
        2       &0.068261 &0.445319&         &        &         \\
        \hline
        3       &-1.700996&0.573735&         &        &         \\
        4       &-1.965399&0.805982&-0.040138&        &         \\
        5       &-1.877128&0.651369&0.024729&-0.007699&         \\
        6       &-1.802422&0.435425&0.191642&-0.055050&0.004442 \\
        \hline
    \end{tabular}
    \caption{Regression analysis for 100k range with phone}
\end{figure}

\begin{figure}[h]
    \centering
    \textbf{HD2102 -- 100k} \\
    \includegraphics[width=8cm]{images/regression/ad4eth-100k-lin.eps}
    \includegraphics[width=8cm]{images/regression/ad4eth-100k-log.eps} \\
    \includegraphics[width=8cm]{images/regression/ad4eth-100k-gamma.eps} \\
    Linear-linear illuminance to voltage, logarithmic-logarithmic illuminance
    to voltage, \\
    $\gamma$ (sensitivity) as a function of logarithmic illuminance \\
    \begin{tabular}{|l|l|l|l|}
        \hline
        Model & $r^2$ & $\gamma$ at 10 and 100 Lux & Observations \\
        \hline
        1 (Solid black) & 0.9647    & (0.4703)      & \\
        2 (Solid red)  & 0.9647    & 0.4692        & \\
        3 (Solid blue) & 0.9784    & 0.4654        & \\
        4 (Dashed black) & 0.9787   & 0.249/0.314   & Gamma low and increasing \\
        5 (Dashed red)  & 0.9814    & -18.2/-8.61   & Doesn't extrapolate \\
        6 (Dashed blue) & 0.9825    & -7.62/-7.28   & Doesn't extrapolate \\
        \hline
    \end{tabular}
    \par
    \textbf{Parameters:} \\
    \begin{tabular}{|l|r|r|r|r|r|}
        \hline
        Model   & $k_0$   & $k_1$  & $k_2$   & $k_3$  & $k_4$
                                                        or $U_{offset}$ \\
        \hline
        1       &0.060115 &0.470314&         &        &0.015700 \\
        2       &0.060926 &0.469216&         &        &         \\
        \hline
        3       &-1.198418&0.465375&         &        &         \\
        4       &-0.592551&0.182959&0.032818 &        &         \\
        5       &44.211228&-31.279125&7.380965&-0.570854&         \\
        6       &19.756969&0.163843&-6.648575&2.080151&-0.181702\\
        \hline
    \end{tabular}
    \caption{Regression analysis for 100k range with HD2102}
\end{figure}

\FloatBarrier

\begin{table}[h]
    \centering
    \begin{tabular}{|l|r|r|r|r|r|r|}
        \hline
        Dataset & \multicolumn{6}{c|}{$\gamma$ at 10 \% max illuminance
                                     for model n} \\
        & 1 & 2 & 3 & 4 & 5 & 6 \\
        \hline
        Phone 1k    & 0.52  & 0.527 & 0.502 & 0.504 &     - &     - \\
        HD2102 1k   & 0.62  & 0.639 & 0.675 & 0.665 & 0.644 & 0.693 \\
        Phone 100k  & 0.447 & 0.445 & 0.574 & 0.485 & 0.480 &     - \\
        HD2102 100k & 0.470 & 0.469 & (!) 0.465 & (!) 0.446 &     - &     - \\
        \hline
    \end{tabular} \\
    The data points from HD2102 100k only cover a narrow range on the
    logarithmic scale.
    \caption{Regression analysis summary: typical sensitivity}
\end{table}

\begin{table}[h]
    \centering
    \begin{tabular}{|p{8cm}|p{8cm}|}
        \hline
        Model & Observations \\
        \hline
        2 (linear scale, $U \propto E^\gamma$, without offset)
        & Baseline; observe that 1 and 2 are swapped in this table \\
        \hline
        1 (linear scale, $U \propto E^\gamma$, with voltage offset)
        & Lower $r^2$ than without the offset, higher $\gamma$ for the
        1k range. There's not much use in compensating for a voltage offset. \\
        \hline
        3 (logarithmic scale, straight line)
        & If there were no errors this would be identical to \#2.
        Errors appear smaller in the upper end and larger in the lower end
        compared to \#2. \\
        \hline
        4 (logarithmic scale, quadratic polynomial)
        & The most suited for future work of all the tested models. It usually
        yields reasonable looking results, has a non-constant $\gamma$ that
        decreses with increasing light intensity but does not reach zero before
        $10^{20}$ lux. \\
        \hline
        5 (logarithmic scale, cubic polynomial)
        & Fits data better than \#4 as expected. $k_3$ appears negative for the
        100k range and positive for the 1k range, it's likely an artifact of
        over-fitting. \\
        \hline
        6 (logarithmic scale, 4th order polynomial)
        & Fits data better than \#5 as expected but appears to have even more
        issues in the gamma plot and when extrapolating.
        It behaves surprisingly well with the data from the 1k range tested
        with the HD2102. \\
        \hline
    \end{tabular}
    \caption{Regression analysis summary: Evaluation of models}
\end{table}

\begin{table}[h]
    \centering
    \begin{tabular}{|p{8cm}|p{8cm}|}
        \hline
        Dataset & Evaluation \\
        \hline
        Phone 1k    & $\gamma$ is low, probably due to non-linear sensor in
                      the phone. Also lots of noise due to method of
                      measurement. \\
        \hline
        HD2102 1k   & Less noise and even high order polynomials behave nicely
                      with the data. $\gamma$ matches what the datasheet for
                      PGM5526 \cite{PGM5526} claims. This will be used for the
                      1k range. \\
        \hline
        Phone 100k  & $\gamma$ is low, probably due to non-linear sensor in
                      the phone. Also lots of noise due to method of
                      measurement. \\
        \hline
        HD2102 100k & Insufficient data to generate a useful regression. \\
        \hline
    \end{tabular} \\
    There is no dataset suitable for the 100k range.
    \caption{Regression analysis summary: Evaluation of datasets}
\end{table}

\FloatBarrier


\subsubsection{Reversing the formula and adding tolerance}

Solve quadratic equation:
$$
    \log_{10}(U) = k_2\log_{10}(E)^2 + k_1\log_{10}(E) + k_0
\rightarrow
    \log_{10}(E) = \frac{-k_1\pm\sqrt{k_1^2-4k_2k_0 + 4k_2\log_{10}(U)}}{2k_2}
$$

{\Large $$ E = 10^{\left(
    \frac{-k_1\pm\sqrt{k_1^2-4k_2k_0 + 4k_2\log_{10}(U)}}{2k_2}
\right)} $$}

Data for the 1k range (HD2102):\\
\begin{tabular}{|l|r|}
    \hline
    $k_0$ & -1.085690 \\
    $k_1$ & 0.789041 \\
    $k_2$ & -0.031058 \\
    \hline
\end{tabular}

Apply numbers:
{\Large $$ E = 10^{\left(
    \frac{-0.789041\pm\sqrt{0.487708 - 0.124232\log_{10}(U)}}{-0.062116}
\right)} $$}

Deduce that plus minus must be plus (which becomes minus) and simplify further:
{\Large $$ E = 10^{\left(
    \frac{0.789041-\sqrt{0.487708 - 0.124232\log_{10}(U)}}{0.062116}
\right)} $$}

To get the accuracy, a percentual tolerance was added large enough to cover
a made up number (90\%) of data points.  The program to add 0.1\% at a time
until the 90\% target is met is in the section ``\nameref{accuracy-program}''.

\FloatBarrier

\begin{figure}[h]
    \centering
    \includegraphics[width=16cm]{images/ad4eth-1k-accuracy.eps} \\
    \includegraphics[width=16cm]{images/ad4eth-1k-result.eps} \\
    For 90\% coverage, a tolerance of 9.4\% is sufficient.
    \caption{Accuracy (AD4ETH \& HD2102, 1k range)}
    \label{fig:accuracy}
\end{figure}

\FloatBarrier
