import pprint
import time

from measure_common import stats, get_analog, get_analog_stats

default_timedomain = {
    'type': 'timedomain',
    'plots': [
        {
            'title': None,
            'xlabel': 'time (ms)',
            'ylabel': 'V_{out} (V)',
            'x-axis': 'time',
            'y-axes': ['Vout'],
            'styles': ['-s']
        },
        {
            'title': None,
            'xlabel': 'time (ms)',
            'ylabel': 'I_{out} (mA)',
            'x-axis': 'time',
            'y-axes': ['Iout'],
            'styles': ['-s']
        },
        {
            'title': None,
            'xlabel': 'time (ms)',
            'ylabel': 'V_s (V)',
            'x-axis': 'time',
            'y-axes': ['Vs'],
            'styles': ['-s']
        },
    ],
    'scalars': {
        'illuminance': None
    },
    'vectors': {
        'time': [],
        'Vout': [],
        'Iout': [],
        'Vs': [],
    }
}

def record_timedomain(dataset, connection, label, *illuminance):
    '''
    '''
    if label in dataset:
        if dataset[label]['type'] != 'timedomain':
            sys.stderr.write('Incompatible\n')
            return
    if label not in dataset:
        dataset[label] = default_timedomain.copy()
    # Load data
    data = eval(pprint.pformat(dataset[label]))
    # Always update plot info
    data['plots'] = default_timedomain['plots'][:]
    # Update scalars and plot titles
    scalars = data['scalars']
    scalars['illuminance'] = stats(illuminance, scalars['illuminance'])
    illum = 'at {:.0f} \\pm {:.0f} Lux'.format(
        scalars['illuminance']['average'],
        scalars['illuminance']['stddev']
    )
    data['plots'][0]['title'] = 'Vout {}'.format(illum)
    data['plots'][1]['title'] = 'Iout {}'.format(illum)
    data['plots'][2]['title'] = 'Vs {}'.format(illum)
    # Sample new data
    start_t = time.time()
    t = 0
    while t < 1:
        Vout, Iout, Vs = get_analog(connection)
        t = time.time() - start_t
        data['vectors']['Vout'].append(Vout)
        data['vectors']['Iout'].append(Iout)
        data['vectors']['Vs'].append(Vs)
        data['vectors']['time'].append(t*1000)
    # Store data
    dataset[label] = data

