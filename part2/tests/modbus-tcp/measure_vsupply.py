import pprint
import sys

from measure_common import stats, get_analog, get_analog_stats

default_vsupply = {
    'type': 'vsupply',
    'plots': [
        {
            'title': None,
            'xlabel': 'Supply voltage (V)',
            'ylabel': 'V_{out} (V) [stddev, min, average, max]',
            'x-axis': 'Vs',
            'y-axes': ['Vout-stddev', 'Vout-min', 'Vout-average', 'Vout-max'],
            'styles': ['-s', 's', '-s', 's']
        },
        {
            'title': None,
            'xlabel': 'Supply voltage (V)',
            'ylabel': 'I_{out} (mA) [stddev, min, average, max]',
            'x-axis': 'Vs',
            'y-axes': ['Iout-stddev', 'Iout-min', 'Iout-average', 'Iout-max'],
            'styles': ['-s', 's', '-s', 's']
        },
    ],
    'scalars': {
        'illuminance': None
    },
    'vectors': {
        'Vs': [],
        'Vout-stddev': [],
        'Vout-min': [],
        'Vout-average': [],
        'Vout-max': [],
        'Iout-stddev': [],
        'Iout-min': [],
        'Iout-average': [],
        'Iout-max': [],
    }
}

def record_vsupply(dataset, connection, label, *illuminance):
    '''
    '''
    if label in dataset:
        if dataset[label]['type'] != 'vsupply':
            sys.stderr.write('Incompatible\n')
            return
    if label not in dataset:
        dataset[label] = default_vsupply.copy()
    # Load data
    data = eval(pprint.pformat(dataset[label]))
    # Always update plot info
    data['plots'] = default_vsupply['plots'][:]
    # Update scalars and plot titles
    scalars = data['scalars']
    scalars['illuminance'] = stats(illuminance, scalars['illuminance'])
    illum = 'at {:.0f} \\pm {:.0f} Lux'.format(
        scalars['illuminance']['average'],
        scalars['illuminance']['stddev']
    )
    data['plots'][0]['title'] = 'Vout {}'.format(illum)
    data['plots'][1]['title'] = 'Iout {}'.format(illum)
    # Sample new data
    while True:
        sys.stderr.write('>> ')
        sys.stderr.flush()
        if sys.stdin.readline() == 'done\n':
            break
        Vout, Iout, Vs = get_analog_stats(connection)
        data['vectors']['Vs'].append(Vs['average'])
        for key in ('stddev', 'min', 'average', 'max'):
            data['vectors']['Vout-'+key].append(Vout[key])
            data['vectors']['Iout-'+key].append(Iout[key])
    # Store data
    dataset[label] = data
