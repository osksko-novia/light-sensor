# Python 2 only (at the moment)

import socket

class ModbusTCP():
    '''
    This is a synchronous Modbus TCP client.
    
    foo = ModbusTCP(address, port=502)
    response = foo.request([unit_id, function, data_bytes ...])
    '''
    def __init__(self, address, port=502):
        self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn.connect((address, port))
        self.sequence_id = 0
    
    def request(self, modbus_request):
        '''
        modbus_request = [
            unit_identifier,
            function_code,
            data_bytes ...
        ]
        '''
        datalen = len(modbus_request)
        modbus_tcp_request_packet = [
            (self.sequence_id >> 8) & 0xff, # Transaction identifier, high
            self.sequence_id & 0xff,        # Transaction identifier, low
            0, 0,                           # Protocol identifier, high & low
            (datalen >> 8) & 0xff,          # Length field, high
            datalen & 0xff,                 # Length field, low
        ]
        modbus_tcp_request_packet += modbus_request
        # send of the request
        self.conn.sendall(''.join(map(chr, modbus_tcp_request_packet)))
        # Response
        response_head = map(ord, self.conn.recv(6))
        response_transaction = response_head[0]*256 + response_head[1]
        response_protocol = response_head[2]*256 + response_head[3]
        response_length = response_head[4]*256 + response_head[5]
        assert response_transaction == self.sequence_id
        assert response_protocol == 0
        # Get the Modbus part of the response
        self.sequence_id += 1
        return map(ord, self.conn.recv(response_length))

    def __del__(self):
        self.conn.close()
