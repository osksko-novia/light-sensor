#!/usr/bin/env python2

import code
import os
import pprint
import sys
import time

from modbustcp import ModbusTCP
from measure_common import stats, get_analog, get_analog_stats

from measure_timedomain import record_timedomain
from measure_illum import record_illum
from measure_vsupply import record_vsupply

'''
dataset = {
    label: {
        'type': type,
        'scalars': {
            key: value,
            ...
        },
        'plots': [
            {
                'title': title,
                'x-axis': column_name,
                'y-axes': [column_name, ...],
                'xlabel': xlabel,
                'ylabel': ylabel,
                'styles': [style, ...],
            },
            ...
        ],
        'vectors': {
            column_name: [data, ...],
            ...
        }
    },
    ...
}
'''

def main():
    command_functions = {
        'illum': record_illum,
        'vsupply': record_vsupply,
        'scope': record_timedomain,
        'load': load_dataset,
        'save': save_dataset,
        'exit': prog_exit,
        'rename': data_rename,
        'del': data_del,
        'plot': plot,
        'list': list_labels,
        'scalars': scalars,
        'help': disp_help,
        'realtime': realtime,
        'connect': connect,
        'cat': merge_data,
        'python': python,
        'shell': shell,
    }
    connection = [None]
    dataset = {}
    while True:
        sys.stderr.write('> ')
        sys.stderr.flush()
        try:
            line = sys.stdin.readline()
        except KeyboardInterrupt:
            sys.stderr.write('\n')
            exit(0)
        if line == '':  # EOF
            sys.stderr.write('\n')
            break
        if line == '\n':
            continue
        parts = filter(None, line.rstrip('\n').split(' '))
        command, args = parts[0], parts[1:]
        if command not in command_functions:
            sys.stderr.write('Unknown command\n')
            continue
        #command_functions[command](dataset, connection, *args)
        try:
            command_functions[command](dataset, connection, *args)
        except TypeError:
            sys.stderr.write('Bad number of arguments\n')

def disp_help(dataset, connection):
    print('''
connect <IP> [<port>]           Connect to AD4ETH
realtime                        Continuosly read analog inputs twice a second
load <filename>                 Load data from file
save <filename>                 Store data to file
illum <label>                   Enter illuminance vs output measuring
vsupply <label> <lux> ...       Enter supply voltage dependence measuring
scope <label> <lux> ...         Measure signals for 1 second
plot <label> [<filename>]       Display or save an Octave plot of <label>
list                            List labels in loaded data
scalars <label>                 Print scalar values belonging to object
rename <old> <new>              Rename label
del <label>                     Delete data by label
cat <label> ... output <label>  Merge data to new label
python                          Interactive Python to deal with stuff manually
shell [command]                 Launch a shell or run a command
help                            This message
exit                            Exit without saving

"illum" measuring
    Constant supply voltage, varying illumination
    Enter illuminance values (*) separated by spaces
    Type "done" to finish
    *) The uncertainty of the illuminance values will be recorded as well
       as the variation in output signals over 1 second

"vsupply" measuring
    Constant illumination, varying supply voltage
    Press enter to take another reading
    Type "done" to finish''')

def merge_data(dataset, connection, *args):
    if len(args) < 3:
        sys.stderr.write('Too few arguments\n')
        return
    if args[-2] != 'output':
        sys.stderr.write('Invalid syntax\n')
        return
    output_label = args[-1]
    input_labels = args[:-2]
    first_input = dataset[input_labels[0]]
    data_type = first_input['type']
    for input_label in input_labels:
        if dataset[input_label]['type'] != data_type:
            sys.stderr.write('Mixed types\n')
            return
    #
    dest = {}
    dest['type'] = data_type
    # Scalars
    dest['scalars'] = {}
    for scalar in first_input['scalars']:
        # Is it stats?
        test = first_input['scalars'][scalar]
        is_stats = False
        if isinstance(test, dict):
            items = {'count', 'min', 'max', 'average', 'stddev'}
            if sorted(items) == sorted(test.keys()):
                is_stats = True
        if not is_stats:
            sys.stderr.write('Warning: Unkown scalar: {}\n'.format(scalar))
            dest['scalars'][scalar] = eval(repr(test))
        else:
            count = 0
            total = 0
            sum_sqr_err = 0
            global_min = test['min']
            global_max = test['max']
            for input_label in input_labels:
                input_stats = dataset[input_label]['scalars'][scalar]
                this_count = input_stats['count']
                count += this_count
                total += this_count * input_stats['average']
                sum_sqr_err += this_count * input_stats['stddev']**2
                global_min = min(global_min, input_stats['min'])
                global_max = min(global_min, input_stats['min'])
            output_stats = {}
            output_stats['count'] = count
            output_stats['average'] = total/count
            output_stats['stddev'] = (sum_sqr_err/count)**.5
            output_stats['min'] = global_min
            output_stats['max'] = global_max
            dest['scalars'][scalar] = output_stats
    # Plots
    sys.stderr.write('Notice: Using plot data from first data\n')
    dest['plots'] = eval(repr(first_input['plots']))
    # Vectors
    dest['vectors'] = {}
    for vector in first_input['vectors']:
        dest['vectors'][vector] = []
        for input_label in input_labels:
            dest['vectors'][vector] += dataset[input_label]['vectors'][vector]
    # Save
    dataset[output_label] = dest
    
def python(dataset, connection):
    code.interact(local=locals())

def shell(dataset, connection, *args):
    if args:
        os.system(' '.join(args))
    else:
        os.system('$SHELL || sh')

def connect(dataset, connection, address, port="502"):
    try:
        connection[0] = ModbusTCP(address, int(port))
    except:
        sys.stderr.write('Connection failed\n')

def realtime(dataset, connection):
    try:
        while True:
            Vout, Iout, Vs = get_analog(connection)
            print('Vout: {:.3f} V\tIout: {:.3f} mA\tVs: {:.1f} V'.format(
                Vout, Iout, Vs))
            time.sleep(0.5)
    except KeyboardInterrupt:
        sys.stderr.write('\n')
        return

def list_labels(dataset, connection):
    sys.stderr.write('{:50} Type\n{:50} ----\n'.format('Name', '----'))
    for label in sorted(dataset.keys()):
        sys.stdout.write('{:50} {}\n'.format(label, dataset[label]['type']))

def scalars(dataset, connection, label):
    try:
        print(pprint.pformat(dataset[label]['scalars']))
    except KeyError:
        sys.stderr.write('No such object: {}.\n'.format(repr(label)))

def plot(dataset, connection, label, *args):
    def matlab(vector):
        '''Convert Python list of floats to Matlab horizontal vector'''
        return '[' + ' '.join(map(str, vector)) + ']'
    try:
        data = dataset[label]
    except KeyError:
        sys.stderr.write('No such object: {}.\n'.format(repr(label)))
        return
    if len(args) > 1:
        sys.stderr.write('Too many arguments')
        return
    elif len(args) == 1:
        filename = args[0]
    else:
        filename = 'tmp-plot'
    try:
        f = open(filename, 'wx')    # Open file only if it doesn't exist
    except:
        sys.stderr.write('Error opening file for writing.\n')
        return
    f.write('#!/usr/bin/env octave\n')
    for fig_index, plot in enumerate(data['plots']):
        f.write('fig{} = figure;\n'.format(fig_index))
        f.write('hold on;\n')
        f.write('[x{0}, x{0}_order] = sort({1});\n'.format(
            fig_index,
            matlab(data['vectors'][plot['x-axis']])
        ))
        for y_index, y_name in enumerate(plot['y-axes']):
            f.write('y{0}_{1} = {2}(x{0}_order);\n'.format(
                fig_index, y_index,
                matlab(data['vectors'][y_name])
            ))
            f.write('plot(x{0}, y{0}_{1}, {2});\n'.format(
                fig_index, y_index, repr(plot['styles'][y_index])
            ))
        f.write('title({});\n'.format(repr(plot['title']).replace(r'\\','\\')))
        f.write('xlabel({});\n'.format(repr(plot['xlabel'])))
        f.write('ylabel({});\n'.format(repr(plot['ylabel'])))
        f.write('grid on;\nhold off;\n')
    for i in range(len(data['plots'])):
        f.write('waitfor(fig{});\n'.format(i))
    f.close()
    os.chmod(filename, 0o755)
    if len(args) == 0:
        os.spawnl(os.P_NOWAIT, filename, filename)
        # It appears that without the delay the script is removed before Octave
        # gets to opening it.
        time.sleep(2)       
        os.unlink(filename)

def load_dataset(dataset, connection, filename):
    try:
        new_dataset = eval(open(filename).read())
    except:
        sys.stderr.write('Load failed\n')
        return
    dataset.clear()
    for key in list(new_dataset):
        dataset[key] = new_dataset[key]
        del new_dataset[key]

def save_dataset(dataset, connection, filename):
    try:
        f = open(filename, 'w')
        f.write(pprint.pformat(dataset))
        f.close()
    except:
        sys.stderr.write('Save failed')

def prog_exit(dataset, connection):
    exit(0)

def data_rename(dataset, connection, old, new):
    if new in dataset:
        sys.stderr.write('Destination label exists\n')
    try:
        dataset[new] = dataset[old]
        del dataset[old]
    except KeyError:
        sys.stderr.write('No such object: {}.\n'.format(repr(old)))

def data_del(dataset, connection, label):
    try:
        del dataset[label]
    except KeyError:
        sys.stderr.write('No such object: {}.\n'.format(repr(label)))


if __name__ == '__main__':
    main()
