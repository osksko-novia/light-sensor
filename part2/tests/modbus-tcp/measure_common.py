import time

def stats(data, old=None):
    data = map(float, data)
    if old is None:
        old = {
            'min': float("inf"),
            'max': float("-inf"),
            'count': 0,
            'average': 0.0,
            'stddev': 0.0,
        }
    result = {}
    result['min'] = min(min(data), old['min'])
    result['max'] = max(max(data), old['max'])
    count = len(data) + old['count']
    result['count'] = count
    new_sum_data = sum(data)
    old_sum_data = old['average'] * old['count']
    avg = (new_sum_data+old_sum_data) / count
    result['average'] = avg
    new_sum_sqr_err = sum([(x-avg)**2 for x in data])
    old_sum_sqr_err = old['stddev']**2 * old['count']
    result['stddev'] = ((new_sum_sqr_err+old_sum_sqr_err)/count)**.5
    return result

def get_analog(connection):
    '''
    Vout, Iout, Vs
    '''
    # TODO
    #for i in range(1000):
    #    i**i
    #return (0.5*time.time())%10, (0.5*time.time())%16+4, (0.25*time.time())%10+19
    # Real code here
    response = connection[0].request([
        0xff,       # Unit ID
        0x04,       # Read input register(s)
        0x00, 0x00, # Start address 0 for channel 1
        0x00, 0x0c  # 12 registers (3 channels * 4 registers per channel)
    ])
    # response[0]               # Unit ID echo
    assert response[1] == 0x04  # Function code echo, high bit indicates error
    assert response[2] == 0x18  # 24 bytes are to be returned
    data = response[3:]
    Vout = (data[2]*256 + data[3]) / 1000.0
    Iout = (data[10]*256 + data[11]) / 500.0
    Vs = 3 * (data[18]*256 + data[19]) / 1000.0
    return Vout, Iout, Vs

def get_analog_stats(connection):
    Vout_array = []
    Iout_array = []
    Vs_array = []
    start_t = time.time()
    while True:
        Vout, Iout, Vs = get_analog(connection)
        timestamp = time.time()
        Vout_array.append(Vout)
        Iout_array.append(Iout)
        Vs_array.append(Vs)
        if timestamp - start_t > 1:
            break
    return stats(Vout_array), stats(Iout_array), stats(Vs_array)

