import pprint
import sys

from measure_common import stats, get_analog, get_analog_stats

default_illum = {
    'type': 'illum',
    'plots': [
        {
            'title': None,
            'xlabel': 'Illuminance (Lux)',
            'ylabel': 'V_{out} (V) [stddev, min, average, max]',
            'x-axis': 'illuminance-average',
            'y-axes': ['Vout-stddev', 'Vout-min', 'Vout-average', 'Vout-max'],
            'styles': ['-s', 's', '-s', 's']
        },
        {
            'title': None,
            'xlabel': 'Illuminance (Lux)',
            'ylabel': 'I_{out} (mA) [stddev, min, average, max]',
            'x-axis': 'illuminance-average',
            'y-axes': ['Iout-stddev', 'Iout-min', 'Iout-average', 'Iout-max'],
            'styles': ['-s', 's', '-s', 's']
        },
        {
            'title': None,
            'xlabel': 'V_{out} (V)',
            'ylabel': 'Illuminance (Lux) [stddev, min, average, max]',
            'x-axis': 'Vout-average',
            'y-axes': ['illuminance-stddev', 'illuminance-min',
                       'illuminance-average', 'illuminance-max'],
            'styles': ['-s', 's', '-s', 's']
        },
        {
            'title': None,
            'xlabel': 'I_{out} (mA)',
            'ylabel': 'Illuminance (Lux) [stddev, min, average, max]',
            'x-axis': 'Iout-average',
            'y-axes': ['illuminance-stddev', 'illuminance-min',
                       'illuminance-average', 'illuminance-max'],
            'styles': ['-s', 's', '-s', 's']
        },
        {
            'title': None,
            'xlabel': 'U_{out} (V)',
            'ylabel': 'I_{out} (mA)',
            'x-axis': 'Vout-average',
            'y-axes': ['Iout-average'],
            'styles': ['-s']
        }
    ],
    'scalars': {
        'Vs': None
    },
    'vectors': {
        'illuminance-stddev': [],
        'illuminance-min': [],
        'illuminance-average': [],
        'illuminance-max': [],
        'Vout-stddev': [],
        'Vout-min': [],
        'Vout-average': [],
        'Vout-max': [],
        'Iout-stddev': [],
        'Iout-min': [],
        'Iout-average': [],
        'Iout-max': [],
    }
}

def record_illum(dataset, connection, label):
    '''
    '''
    if label in dataset:
        if dataset[label]['type'] != 'illum':
            sys.stderr.write('Incompatible\n')
            return
    if label not in dataset:
        dataset[label] = default_illum.copy()
    # Load data
    data = eval(pprint.pformat(dataset[label]))
    # Always update plot info
    data['plots'] = default_illum['plots'][:]
    # Update scalars and plot titles
    scalars = data['scalars']
    # TODO: Vs is replaced by the latest run
    #scalars['Vs'] = stats(get_analog_stats(connection)[2], scalars['Vs'])
    scalars['Vs'] = get_analog_stats(connection)[2]
    Vs = 'V_s is {:.1f} \\pm {:.2f} V'.format(
        scalars['Vs']['average'],
        scalars['Vs']['stddev']
    )
    data['plots'][0]['title'] = 'Illuminance/V_{out}, ' + Vs
    data['plots'][1]['title'] = 'Illuminance/I_{out}, ' + Vs
    data['plots'][2]['title'] = 'V_{out}/Illuminance, ' + Vs
    data['plots'][3]['title'] = 'I_{out}/Illuminance, ' + Vs
    data['plots'][4]['title'] = 'V_{out}/I_{out}, ' + Vs
    # Sample new data
    while True:
        sys.stderr.write('>> ')
        sys.stderr.flush()
        line = sys.stdin.readline()
        if line == 'done\n':
            break
        try:
            illuminance = stats(line.rstrip('\n').split(' '))
        except:
            sys.stderr.write('Bad input\n')
            continue
        Vout, Iout, _ = get_analog_stats(connection)
        for key in ('stddev', 'min', 'average', 'max'):
            data['vectors']['illuminance-'+key].append(illuminance[key])
            data['vectors']['Vout-'+key].append(Vout[key])
            data['vectors']['Iout-'+key].append(Iout[key])
    # Store data
    dataset[label] = data
