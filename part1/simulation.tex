\begin{nopbrk}
    \section{Simulations}
    The simulations are transient simulations using time as a variable for
    calculating LDR resistance.  The simulated illuminance starts at 1 Lux
    and increases by 10 Lux/s.
    \begin{align*}
        & E = 1\ Lux + t \cdot 10\ \frac{Lux}{s}
    \end{align*}
    Do note that this causes some visible distortion as 100 ms corresponds to
    2 Lux rather than 1.

    \subsection{Default simulation}

    Various signals when simulating the complete LTspice circuit with default
    parameters: \\
    \includegraphics[width=17cm]{images/sim/default.png}
    
    1 Lux to 110 000 Lux log scale, 0.01 V to 30 V log scale.

    This is the simulation of all external signals except
    \code{analog\_current} in a log-log scale.
    
    \begin{itemize}
        \item \code{range\_select} goes from low to high at 1100 Lux (110 s)
        \item \(\gamma = 0.7\)
        \item \(R_{10} = 10\ k\Omega\)
        \item \(level = 2\ k\Omega\) (20\% trigger level for comparator output)
        \item \(hyst = 100\ k\Omega\)
        \item Load on \code{analog\_voltage} is 500Ω, the maximum allowed
        \item Digital signals offset to avoid overlap.
    \end{itemize}
\end{nopbrk}

\begin{nopbrk}    
    \subsection{Supply voltage independence}
    Analog voltage and current, and \code{over\_range} at varying voltages
    overlapped.

    The analog signals should overlap as one black line. \code{over\_range}
    is separated as it is a digital signal that goes only as high as the
    supply voltage.

    \begin{tabular}{|l|l|}
        \hline
        Cyan component    & 15 V * \\
        Magenta component & 19 V   \\
        Yellow component  & 29 V   \\
        \hline
    \end{tabular}
    
    * The 15V is specific to the simulated circuit. The LM358 may have up to
    4 volts drop in the worst case and the 7815 can output as low as 14.25 V.
    The LTspice simulation uses generic (ideal) op-amps which are rail-to-rail.
    15 V in the simulation is equivalent to 19 V in real life with the worst
    case LM358s and 7815.

    For maximum stress at lower supply voltages:
    \begin{itemize}
        \item 20 mA on voltage output
        \item 10 V on current output
        \item \(\gamma = 1\) and \(R_{10} = 8\ k\Omega\)
    \end{itemize}    

    \code{range\_select} is used just like in the default simulation, but the
    trace is not shown to remove clutter.   The curvy trace is the current.

    \includegraphics[width=17cm]{images/sim/supply-voltage.png} \\
    6 Lux to 110 000 Lux (500 ms to 11 ks)

    There seems to be an about 6\% error on the voltage below 10 Lux, but it
    becomes imperceptible after 30 Lux.  Rather counter-intuively, a lower
    supply voltage yields a higher signal voltage.

    The current output has very little error.
\end{nopbrk}

\begin{nopbrk}    
    \subsection{Power/current draw}
    For maximum current consumption:
    \begin{itemize}
        \item 20 mA current draw on \code{analog\_voltage} and 100 mA on each
              of the digital outputs
        \item Illumination up to 200 000 Lux
        \item \code{\textoverline{digital}} forced to be always high
        \item \(\gamma=1\) and \(R_{8\ k\Omega}\) for maximum current through
              the LDR.
        \item Simulation is run at 29 V
        \item Simulation has current sources to account for the quescient
              current in the 358s and the 7815 (in \code{7815.asc}).
    \end{itemize}

    \(level\) and \(hyst\) have been set up to cause the comparator to latch
    up with a trigger level above 100\% to cause both digital outputs to be
    high simultaneously:
    \begin{align*}
        & level = 8\ k\Omega \\
        & hyst = 0\ k\Omega \\
        & V_{trig,high} = \frac{
              \frac{25\ V}{47\ k\Omega + hyst}
              + \frac{2.5\ V}{10\ k\Omega - level}
              + \frac{0\ V}{level}
          }{
              \frac{1}{47\ k\Omega + hyst}
              + \frac{1}{10\ k\Omega - level}
              + \frac{1}{level}
          } = 2.76\ V
    \end{align*}
    At the beginning \code{\textoverline{digital}} will go high because
    near zero illuminance is below the trigger level.  As soon as the output
    goes high the actual trigger level will be bumped to 110\%.

    \includegraphics[width=17cm]{images/sim/power.png}

    \code{range\_select} is used just like in the default simulation, but the
    trace is not shown to remove clutter.

    The two digital signals are monitored to ensure they both are high.

    Current consumption is below 340 mA, of which 220 mA is spent on voltage
    outputs.
\end{nopbrk}

\begin{nopbrk}
    \subsection{Transfer function}
    %C,MY  0-1000 0-100000  0.5
    %C,MY  0-1000 0-100000  0.75
    %C,MY  0-1000 0-100000  1

    Curves of the transfer function (light to voltage) through the range of
    gamma characterstics: \\
    \begin{tabular}{|l|l|}
        \hline
        Cyan    & \(\gamma = 0.5\) \\
        Magenta & \(\gamma = 0.75\) \\
        Yellow  & \(\gamma = 1\) \\
        \hline
    \end{tabular}
    
    0 to 1000 Lux on the left, 0 to 100 000 Lux on the right with a thin line
    marking 1000 Lux: \\
    \includegraphics[width=7.4cm]{images/sim/lin-1k.png}
    \includegraphics[width=7.4cm]{images/sim/lin-100k.png}
    \\
\end{nopbrk}
\begin{nopbrk}
    0 to 2000 Lux in the 0 to 100 000 Lux scale, at 1000 Lux (100 s) the
    voltage would be 10 V in the 1000 Lux scale: \\
    \includegraphics[width=7.4cm]{images/sim/lin-2k.png} \\
    \par
    The 2000 Lux and 100 000 Lux graphs were taken with \code{range\_select}
    held to 24 V the entire time, and the 1000 Lux graphs with 0 V.
    \\
\end{nopbrk}
\begin{nopbrk}
    \subsubsection{Current output}
    Current output through various loads: \\
    \begin{tabular}{|l|l|}
        \hline
        Cyan    & short \\
        Magenta & 250 Ω \\
        Yellow  & 10 V \\
        \hline
    \end{tabular}
    
    Simulated U to I transfer on the left, deviation from intended current on
    the right: \\
    \includegraphics[width=7.4cm]{images/sim/current.png}
    \includegraphics[width=7.4cm]{images/sim/current-error.png}
    \\
    The voltage to current simulation was done using only the U-to-I part
    with a DC analysis.  \(V_S\) is 24 V.
    \\
\end{nopbrk}

\begin{nopbrk}    
    \subsection{Ringing without C14}
    Very picky.  Works fine with 24 V or with a 120 seconds long simulation.
    Using 19 V and \code{.tran 200} is sufficient to cause ringing in the
    simulation.

    Ringing occurs when the differential signal falls back to below 10 volts:\\
    \includegraphics[width=7.4cm]{images/sim/C14-zoom1.png}
    \\
\end{nopbrk}
\begin{nopbrk}
    Amplitude and duration: \\
    \includegraphics[width=17cm]{images/sim/C14-zoom2.png} \\
    4.75 volts peak to peak at worst, at or above 3 V p-p for about 2.3
    milliseconds.  There also seems to be something clamping it to 11.5 volts.
    \\
\end{nopbrk}
\begin{nopbrk}
    Frequency (at the end): \\
    \includegraphics[width=17cm]{images/sim/C14-zoom3.png} \\
    Looks to be about 1.25 MHz between ...770 and ...780.
    It may look like the frequency suddenly decreases but that's not the case.
    \\    
\end{nopbrk}
\begin{nopbrk}
    Adding C14 removes the ringing almost completely: \\
    \includegraphics[width=7.4cm]{images/sim/C14-fixed-zoom0.png}
    \\
\end{nopbrk}
\begin{nopbrk}
    Zooming in on the tiny blip at 110s 94ms 798µs: \\
    \includegraphics[width=7.4cm]{images/sim/C14-fixed-zoom1.png} \\
    The amplitude appears to be about 50 µV p-p.  Looks like one cycle of
    about 500 kHz to 1 MHz.
    \\
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{Ringing on \texttt{over\_range}}
    A very small blip on \code{over\_range} at the start of the ringing on
    \code{analog\_voltage}: \\
    \includegraphics[width=7.4cm]{images/sim/C14-digital-zoom0.png}
    \\
\end{nopbrk}
\begin{nopbrk}
    The blip and some delayed ringing visible when zooming in: \\
    \includegraphics[width=17cm]{images/sim/C14-digital-zoom2.png} \\
    It appears that whatever clamping happened at 11.5 V on
    \code{analog\_voltage} kept the ringing silenced.
    \\
\end{nopbrk}
\begin{nopbrk}
    The blip zoomed in: \\
    \includegraphics[width=7.4cm]{images/sim/C14-digital-zoom3a.png}
    \\
\end{nopbrk}
\begin{nopbrk}
    The ringing zoomed in: \\
    \includegraphics[width=17cm]{images/sim/C14-digital-zoom3b.png}
    \\
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{Rising edge}
    Could slowing down U5 cause problems on the rising edge?
    
    \update{\textbf{This simulation was done with C14 at 10 pF, but it should
    have been 100 pF.} Because of this mistake, this simulation may be faulty.}

    With C14 on the left, without C14 on the right: \\
    \includegraphics[width=7.4cm]{images/sim/C14-fixed-rising.png}
    \includegraphics[width=7.4cm]{images/sim/C14-unfixed-rising.png} \\
    There is some overshoot but only on the digital signal and independent
    of the addition of C14.

    Intentionally slowing down U5 does not seem to cause any issues.
\end{nopbrk}


\begin{nopbrk}
    \subsection{Simulation parameters}
    These are the parameters set in the complete LTspice schematic.

    \begin{itemize}
        \item Some of the loads can vary between constant resistance, constant
              current and constant voltage
        \item PWL refers to a waveform function that is 0 V from 0 Lux to
              1 100 Lux and then switches to 24 V
        \item A star indicates that it's not a fixed value, see the section for
              the specific simulation for details
        \item \code{.tran} sets the transient analysis time because the LDR is
              simulated through time: \\
              \(E = 1\ Lux + t \cdot 10\frac{Lux}{s}\)
    \end{itemize}
    
    \begin{tabular}{|l|rrrrr|}
        \hline
                             & Default & Voltage & Power  & Transfer  & C14    \\
        \hline
        \code{supply}        & 24 V    & *       & 29 V   & 24 V      & 19 V   \\
        \hline
        \code{LDRy}          & 0.7     & 1.0     & 1.0    & *         & 0.7    \\
        \code{LDR10}         & 10 kΩ   & 8 kΩ    & 8 kΩ   & 10 kΩ     & 10 kΩ  \\
        \hline
        \code{load1}         & 500 Ω   & 20 mA   & 20 mA  & 500 Ω     & 500 Ω  \\
        \code{load2}         & 1 Ω     & 10 V    & 0 V    & 1 Ω       & 1 Ω    \\
        \code{load3}         & 1 MΩ    & 1 MΩ    & 100 mA & 1 MΩ      & 1 MΩ   \\
        \code{load4}         & 1 MΩ    & 1 MΩ    & 100 mA & 1 MΩ      & 1 MΩ   \\
        \hline
        \code{level}         & 2 kΩ    & 2 kΩ    & 8 kΩ   & 2 kΩ      & 2 kΩ   \\
        \code{hyst}          & 100 kΩ  & 100 kΩ  & 0.01 Ω & 100 kΩ    & 100 kΩ \\
        \hline
        \code{.tran}         & 11000   & 11000   & 20000  & *         & 200    \\
        \code{range\_select} & PWL     & PWL     & PWL    & *         & PWL    \\
        \hline
    \end{tabular}
\end{nopbrk}


\begin{nopbrk}
    \subsection{\texttt{range\_select}}
    It is not known how M1 (BS170) will behave at low input voltages. It will
    likely conduct far too hard at its threshold.

    This is a simulation using BSS123 which appears to be  somewhat similar. \\
    \begin{tabular}{|l|rr|l|}
        \hline
        \textbf{Parameter}       & \textbf{BS170} & \textbf{BSS123} & \textbf{unit} \\
        \hline
        max \(I_{DSS}\) (25 °C)  & 0.5            & 1               & µA \\
        max \(I_{DSS}\) (125 °C) & -              & 60              & µA \\
        test \(V_{DS}\)          & 25             & 100             & V \\
        \hline
        max \(R_{DS(on)}\)       & 5              & 6               & Ω \\
        typ \(R_{DS(on)}\)       & 1.8            & 1.2             & Ω \\
        test \(V_{GS}\)          & 10             & 10              & V \\
        \hline
        min \(V_{GS(th)}\)       & 0.8            & 0.8             & V \\
        typ \(V_{GS(th)}\)       & 2.0            & 1.7             & V \\
        test \(I_D\)             & 1              & 1               & mA \\
        test \(V_{DS}\)          & \(V_{GS}\)     & \(V_{GS}\)      & V \\
        \hline
        min \(g_{FS}\)           & -              & 0.08            & S \\
        typ \(g_{FS}\)           & 0.2            & 0.8             & S \\
        \hline
    \end{tabular}
    \\
\end{nopbrk}
\begin{nopbrk}
    P5 has been adjusted for 5\% of 2.5 V. The output voltage should stay
    at either 2.5 V or 125 mV.  Y-axis is the output voltage, X-axis is
    \code{range\_select}: \\
    \includegraphics[width=7.4cm]{images/sim/range-select-1.png}
    
    For a typical BSS123 and no tolerance issues on the resistors, etc: \\
    \begin{tabular}{|l|r|}
        \hline
        \(V_{low,max}\)  & 1.96 V \\
        \(V_{high,min}\) & 2.94 V \\
        \hline
    \end{tabular}

    The saturation voltage of a BJT is probably low enough to not cause any
    issues.
    \\
\end{nopbrk}
\begin{nopbrk}
    Input current draw (typical, at nominal resistance and \(V_S = 24\ V\)):\\
    \includegraphics[width=7.4cm]{images/sim/range-select-0.png}
    \\
\end{nopbrk}
\begin{nopbrk}
    This simulation used just the part that divides the reference voltage
    for the two different scales. \\
    \includegraphics[width=7.4cm]{images/sim/range-select-schematic.png}
    \\
\end{nopbrk}


\begin{nopbrk}
    \subsection{Hysteresis for \texttt{\textoverline{digital}}}
    Graphs of low and high trigger voltage through all parameters.
    If the high value exceeds 10 V, the comparator will lock up
    if it ever gets darker than the low trigger level.

    P8 sets the level between 0 and 10 V.  P9 set the hysteresis
    between 1 MΩ and 0 Ω, P9 is always in series with 47 kΩ.

    These are not actual simulations, but graphs for the functions
    listed for \code{\textoverline{digital}}
    in section 6.3 - Transfer functions.

    The LM358s limited ability to sink current at low output voltages
    has not been considered, but most of the hysteresis is on the
    upper value anyway.

    \subsubsection{Logarithmic scale for P9 values}
    To get R25+P9 to be 47 kΩ to 1047 kΩ on a logarithmic scale
    \begin{align*}
        & x = \sqrt[n-1]{\frac{1047\ k\Omega}{47\ k\Omega}} \\
        & R_i = 47\ k\Omega \cdot x^i - 47\ k\Omega
    \end{align*}
    Where \(n\) is the number of levels and \(i\) is from \(0\) to \(n-1\).
    \(R_i\) is the value of P9.

    This is what has been used to calculate the fixed values for P9 for the
    graphs below.
\end{nopbrk}
\begin{nopbrk}
    % 47 * ((1047/47)**.2)**n - 47
    \subsubsection{Set level vs actual level, different levels of hysteresis}
    Op-amp high output voltage is 22 V.  Six levels of hysteresis.
    
    \includegraphics[width=5.5cm]{{images/hysteresis/level-1000-22.0}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/level-516-22.0}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/level-256-22.0}.png}
    \\
    \includegraphics[width=5.5cm]{{images/hysteresis/level-116-22.0}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/level-40-22.0}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/level-0-22.0}.png}
\end{nopbrk}
\begin{nopbrk}
    \subsubsection{Hysteresis control, different set levels}
    Amount of actual hysteresis vs P9 position at five different set trigger
    levels.
    
    \includegraphics[width=5.5cm]{{images/hysteresis/hyst-2.5-22.0}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/hyst-5.0-22.0}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/hyst-7.5-22.0}.png}
    \\
    \includegraphics[width=5.5cm]{{images/hysteresis/hyst-0.5-22.0}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/spacer}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/hyst-9.5-22.0}.png}
\end{nopbrk}
\begin{nopbrk}
    % 47 * ((1047/47)**.5)**n - 47
    \subsubsection{Hysteresis vs op-amp output voltage}
    Effects of supply voltage and op-amp drop out on hysteresis.
    Set voltage at 5 V for high impact and 1 V for low impact.
    
    \includegraphics[width=5.5cm]{{images/hysteresis/voh-5.0-1000}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/voh-5.0-175}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/voh-5.0-0}.png}
    \\
    \includegraphics[width=5.5cm]{{images/hysteresis/voh-1.0-1000}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/voh-1.0-175}.png}
    \includegraphics[width=5.5cm]{{images/hysteresis/voh-1.0-0}.png}
\end{nopbrk}
