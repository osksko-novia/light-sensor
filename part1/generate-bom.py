#!/usr/bin/python3

# Component     Amount      Unit price      Min quantity    Optional    Comments
BOM = [
('1N4148',      6,          0.05,          10,              False,      ''),
('1598B',       1,         12.80,           1,              False,      ''),
('22-01-2025',  8,          0.20,           5,              False,      ''),
('22-01-S',    16,          0.10,          10,              False,      ''),
('22-27-2021',  7,          0.24,           5,              False,      ''),
('6390B',       1,          2.80,           1,              False,      ''),
('7815CP',      1,          0.45,           1,              False,      ''),
('BC327',       2,          0.09,           5,              False,      ''),
('BC337',       5,          0.10,           5,              False,      ''),
('BS170',       1,          0.20,           5,              False,      ''),
('HTC5S',       1,          2.90,           1,              True,       'Thermal grease, only if not already available. Probably not needed'),
#'LM358N',      5,          0.40,           1,              False,      ''),
('P10-10K',     1,          0.45,           1,              False,      ''),
('P10-1M',      1,          0.50,           1,              False,      ''),
#'PE12-2.5',    1,          1.36,           1,              False,      'For connecting test equipment'),
('PGM5526',     1,          0.25,           5,              False,      'LDR'),
('TL431CLP',    1,          0.30,           1,              False,      ''),
('T910Y-5K',    2,          0.40,           1,              False,      ''),
('V10-250R',    1,          0.45,           1,              False,      ''),
('V10-1K',      1,          0.40,           1,              False,      ''),
('V10-100K',    3,          0.40,           1,              False,      ''),
]
BOM2 = [
('Screws: 6-32 UNC',                                    1,          'Can supply myself'),
('Screws: Self-tapping UTS #6 x 1/4" (M3.5 x 6.4mm)',   3,          'For mounting PCB'),
('Total length of wires required, 0.35 mm^2',           '197.5cm',  '6x 2x 10cm for PWR and I/O + 1x 2x 30cm for LDR + PCB links and wires (17.5 cm)'),
('Some thin rigid tube to glue LDR in',                 1,          'Can supply myself'),
]

def escape(s):
    if s:
        return '"{}"'.format(s.replace('"', '""'))
    else:
        return ""

f = open('bom.csv', 'w')
f.write('Part,Needed quantity,Minimum purchase quantity,Purchase quantity (worst case),Unit cost,Cost (worst case),Comments\n')
total = 0
total2 = 0
for part, amount, unit_cost, min_quantity, optional, comment in BOM:
    purchase_amount = ((amount-1)//min_quantity + 1) * min_quantity
    cost = unit_cost * purchase_amount
    total += cost
    if not optional:
        cost2 = unit_cost * amount
        total2 += cost2
        cost2 = '{:.2f}'.format(cost2)
    else:
        cost2 = '---'
    f.write('{},{},{},{},{:.2f},{:.2f},{}\n'.format(
        part,
        amount,
        min_quantity,
        purchase_amount,
        unit_cost,
        cost,
        #cost2,
        escape(comment)
    ))
for thing, amount, comment in BOM2:
    f.write('{},{},---,---,---,---,{}\n'.format(
        escape(thing),
        amount,
        escape(comment)
    ))
f.write('Total,,,,,{:.2f},\n'.format(total))
f.close()
