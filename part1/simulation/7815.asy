Version 4
SymbolType BLOCK
RECTANGLE Normal -80 -32 80 48
WINDOW 0 8 -40 Bottom 2
PIN -80 0 LEFT 8
PINATTR PinName Vin
PINATTR SpiceOrder 1
PIN 0 48 BOTTOM 8
PINATTR PinName Gnd
PINATTR SpiceOrder 2
PIN 80 0 RIGHT 8
PINATTR PinName Vout
PINATTR SpiceOrder 3
