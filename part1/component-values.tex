\begin{nopbrk}
    \subsection{Component selections}
    Resistors are limited to E6 values and capacitors to E3. Resistors are
    assumed to be of 5\% tolerance and 250 mW maximum power dissipation.
    
    \subsubsection{Op-amps}
    The pretty generic LM358 or functionally equivalent LM324 should suffice.
    Three of the op-amps will sink current all the way down to ground, but
    can be managed by using very large resistors. The input bias current is
    approximately three order of magnitude smaller than the maximum sink
    current (at 0V) and can therefore be ignored.

    Maximum output voltage is at least \(V_s - 4\ V\), meaning that they can
    deliver up to 15 V at minimum supply voltage. \\
    Maximum current sink at very low voltage (200 mV) is at least 12 µA.
    % \\ \href{https://www.ti.com/lit/ds/symlink/lm358-n.pdf}{Datasheet}

    TI's datasheet for the LM358 recommends a 10 nF bypass capacitor (C6-C10)
    per chip.
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{\texttt{range\_select}}
    M1 is a BS170 or any other normal N-channel MOSFET in a TO-92 with a DGS
    pinout.  \(R_{DS(on)}\) is at most 5Ω.

    % Round percentage down
    R5 (6.8k) + P1 (1k) forms a voltage divider with R5 to limit the amount
    of useless range.  The useful range is 1\% to 10\% (explained in the
    previous chapter) and R5 sets the upper limit to somewhere between
    \(\frac{0+90\%\cdot1000}{0+90\%\cdot1000+105\%\cdot6800+10} = 11.1\%\) and
    \(\frac{5+110\%\cdot1000}{5+110\%\cdot1000+95\%\cdot6800} = 14.6\%\).

    % Round percantage up
    The lower limit is determined by the \(R_{DS(on)}\) of the MOSFET and
    resistance of the wiring.  The resistance of the wiring on the transistor
    side of the potentiometer should be well below one Ohm, or there are
    bigger problems.  The lower limit is at most
    \(\frac{5+1}{5+1+90\%\cdot1000+95\%\cdot6800} = 0.082\%\).

    The BS170 has a drain cutoff current up to 0.5 µA which is a lot less
    than \(2.5\ V / 7.8\ k\Omega = 320\ \mu{A}\).

    P1 is later chosen as a carbon trimmer potentiometer. To minimize
    thermal drift, R5 should preferrably be a carbon film resistor to match
    the temperature coefficient.

    R2, C11, D1 and D2 offer ESD protection for the MOSFET. C11 should be
    placed as close to the input as possible. The RC filter will allow basic
    diodes like 1N4148s or 1N4001s to be used.
    \\
\end{nopbrk}
\begin{nopbrk}
    R2, R3 and R4 form a voltage divider to take a 19 to 29 volt input signal
    and reduce it to something suitable for a 10 V (\(V_{GS(on)}\)) to 20 V
    \(V_{GS(max)}\) MOSFET.  R2 and the diodes form a voltage clamp that
    enforces the voltage across R3 and R4 to stay between -0.7 and 29.7 volts.
    \par
    1, 10 and 15 kilo ohms are appropriate values for this MOSFET or any
    similar.
    \begin{align*}
        & R2 = 1\ k\Omega, R3 = 10\ k\Omega, R4 = 15\ k\Omega \\
        & V_{max} = 30\ V \cdot
                    \frac{105\%\cdot R4}{95\%\cdot R3 + 105\%\cdot R4}
                    = 18.72\ V \\
        & V_{min} = 19\ V \cdot
                    \frac{95\%\cdot R4}{105\%(R2+R3) + 95\%\cdot R4}
                    = 10.49\ V
    \end{align*}
    \\
\end{nopbrk}
\begin{nopbrk}
    \textbf{Bug:}
    \(V_{GS(th)}\) for the BS170 is specified at \(I_D = 1\ mA\).
    The current through the divider when the transistor is fully on is
    only 320 µA - while it is high enough to not make the up to 0.5 µA
    drain cut-off current a concern and low enough to not make the
    5 Ω \(R_{DS(on}\) another - it is well below the threshold drain
    current.

    The circuit should be fixed to drive the FET hard.  But in the current
    form the issue can be mitigated by specifying \(V_{low,max} = 0\ V\).
    
    The resistance values above give the following logic level voltage ranges:
    \begin{align*}
        & V_{GS(on)} = 10\ V,\ \ V_{GS(th,min)} = 0.8\ V \\
        & Rn_{min} = 95\%Rn,\ \ Rn_{max} = 105\%Rn \\
        & P_{R1,max} = 250\ mW \\
        & V_{high,max} = V_S + \sqrt{P_{R1,max}\cdot R1_{min}} + 0.6\ V
                       = V_S + 16\ V\\
        & V_{high,min} = \frac{R2_{max} + R3_{max} + R4_{min}}{R4_{min}}
                         \cdot V_{GS(on)}
                       = 18.2\ V \\
        & V_{low,th} = \frac{R2_{min} + R3_{min} + R4_{max}}{R4_{max}}
                       \cdot V_{GS(th,min)}
                     = 1.3\ V \\
        & V_{low,max} = 0\ V \\
        & V_{low,min} = - \sqrt{P_{R1,max}\cdot R1_{min}} - 0.6\ V
                      = - 16\ V
    \end{align*}
    \(V_{low,th}\) is the lowest voltage that could get the gate of the FET
    to its threshold voltage at which it may have an effective resistance
    in the proximity of \(0. 8\ V / 1\ mA = 800\ \Omega\).
    \\
\end{nopbrk}
\begin{nopbrk}
    C11 also serves to filter noise. With a capacitance much higher than
    in the MOSFET, high frequency noise will be shunted to ground.

    C11 must not be too large. The LDR has a rise time of 20 ms and
    \code{range\_select} should be faster than that.  The time constant
    should be kept at least an order of magnitude lower than 20 ms.
    \begin{align*}
        & 10\tau < 20\ ms \\
        & \tau = (R3 + R4) \cdot C11 \\
        & C11 < \frac{2\ ms}{15\ k\Omega} \\
        & C11 < 133\ nF \\
    \end{align*}
    %\todo{How many time constants are actually needed to swith in worst case?}
    % Can't calculate that without knowing the actual upper limit of logic low.
\end{nopbrk}





\begin{nopbrk}
    \subsubsection{Bipolar junction transistors and their current limiting
                   resistors}
    Q1 to Q7 are assumed to have an \(h_{FE} \geq 100\).
    BC337 for NPNs and BC327 for PNPs or any other TO-92 with a CBE pinout,
    \(h_{FE} \geq 100\) and a max power dissipation at 70 °C of at least
    400 mW.
    \begin{align*}
        & V_{opamp} = V_{S,min} - 4\ V = 15\ V \\
        & h_{FE} = 100
    \end{align*}
    
%\end{nopbrk}
%\begin{nopbrk}
    \paragraph{LDR: Q1, R6}
    R6 limits the current from U3 to prevent it from putting much more than
    15 V on P2 + P3.
    \begin{align*}
        & V_{e,max} = 12.5\ V \\
        & I_{e,max} = I_{LDR,full} = 32\ mA \\
        & R6_{max} = h_{FE} \cdot \frac{V_{opamp} - V_{e,max} - 0.7\ V}
                                       {I_{e,max}}
                   = 100 \cdot \frac{15\ V - 12.5\ V - 0.7\ V}{32\ mA}
                   = 5.6\ k\Omega
    \end{align*}
    The chosen 3.3 kΩ limits the current from U3 during
    LDR short / over illumination to less than
    \((29\ V-0.6\ V) / (3.3\ k\Omega + 320\ \Omega) = 7.9\ mA\)

    The maximum (LDR shorted and P2+P3 at 320Ω, the minimum)
    power dissipation in Q1 is:
    \begin{align*}
        & \frac{(15.75\ V/2)^2}{320\ \Omega} + 7.9\ mA\cdot 0.7\ V = 200\ mW
    \end{align*}
\end{nopbrk}
\begin{nopbrk}
    \paragraph{\texttt{analog\_voltage}: Q2, R11, R24}
    
    R11 limits the base current to Q2 / current from U5.
    \begin{align*}
        & V_{e,max} = 10\ V \\
        & I_{e,max} = 20\ mA \\
        & R11_{max} = h_{FE} \cdot \frac{V_{opamp} - V_{e,max} - 0.7\ V}
                                        {I_{e,max}}
                   = 100 \cdot \frac{15\ V - 10\ V - 0.7\ V}{20\ mA}
                   = 21.5\ k\Omega
    \end{align*}
    The chosen 3.3 kΩ limits the current from U5 during output short circuit
    to:
    \begin{align*}
        & (29\ V - 0.6\ V) / 3.3\ k\Omega = 8.7\ mA
    \end{align*}
    \\
\end{nopbrk}
\begin{nopbrk}
    R24 limits the power dissipation in Q2 during output short circuit.
    \begin{align*}
        & V_{e,max} = 10\ V \\
        & I_{e,max} = 20\ mA \\
        & V_{15V,min} = 14.25\ V \\
        & V_{ce,sat} = 0.5 \ V \\
        & R24_{max} = \frac{V_{15V,min} - V_{ce,sat} - V_{e,max}}{I_{e,max}}
                    = 187.5\ \Omega
    \end{align*}
    R24 is 150 ohms.
    The maximum power in Q2 is:
    \begin{align*}
        & V_{15V,max} = 15.75\ V \\
        & P(I) = I\cdot(V_{15V,max} - I\cdot R24_{min}) \\
        & P'(I) = V_{15V,max} - 2 \cdot I\cdot R24_{min} \\
        & P'(I) = 0 \rightarrow I =\frac{15.75\ V}{2\cdot95\%\cdot150\ \Omega}
                                  = 55.3\ mA \\
        & P_Q = 55.3\ mA \cdot (15.75\ V - 55.3\ mA\cdot95\%150\ \Omega)
            = 436\ mW
    \end{align*}
    436 mW is barely more than the maximum 400 mW the transistor can tolerate.
    \par
    The maximum power in R24 during a dead short is:
    \begin{align*}
        & P_R = V_{15V,max}^2 / R24_{min} = 1.741\ W
    \end{align*}
\end{nopbrk}
\begin{nopbrk}
    \paragraph{\texttt{analog\_current}: Q5}
    The power dissipation in Q5 for \code{analog\_current} will not exceed:
    \begin{align*}
        & P = (15.75\ V - 0.6\ V) \cdot 20\ mA + 0.7\ V \cdot 0.2\ mA
            = 304 mW
    \end{align*}
    The 0.6 V voltage drop is from D6.
\end{nopbrk}
\begin{nopbrk}
    \paragraph{Digital outputs: Q3, Q4, Q6, Q7, R12, R13, R22, R23}

    R12 and R13 are chosen to be 47 kΩ and 3.3 kΩ to have a digital output
    with low dropout (well saturated) with a max load of 100 mA.
    (The exact calculations have been forgotten.)

    R22 and R23 are the same as R12 and R13.
\end{nopbrk}



\begin{nopbrk}
    \subsubsection{Fixed voltage dividers (resistors and some potentiometers)}
    Half of these are listed as 22 k, but the actual value doesn't as long as
    the ratio is correct and they are in a sane range (eg. 10k - 100k).
    
    The pairs (R7, R8), (R9, R10), (R14, R15), (R16, R17), (R18, R19) and
    (R20, R21) are all 50\% dividers with varying requirements of accuracy:
    \begin{itemize}
        \item The ratio between R7:R8 (22k) and R9:R10 (470k) is critical.
        \item The error from R9:R10 (same as R7:R8) can be calibrated away
              with P2 plus P3.
        \item The error from R14:R15 can be calibrated away with P5 and P6.
        \item The error from R16:R17 can be calibrated away with P5, P6 and
              P7.
        \item The ratio R18:R19 (470k) is critical.
        \item The ratio R20:R21 (22k) is critical.
    \end{itemize}

    The pairs (R9, R10), (R14, R15) and (R18, R19) are 50\% dividers that need
    to have a high resistance as the op-amps may in a worst case scenario only
    be able to sink 12 µA.

    Minimum values depends on maximum voltage across the entire divider:
    \begin{align*}
        & 12\ \mu{A} \geq \frac{10\ V - 0.025\ V}{R9 + R10}
            \rightarrow R9 \geq 416\ k\Omega \\
        & 12\ \mu{A} \geq \frac{2\ V - 0.5\ V}{R14 + R15}
            \rightarrow R14 \geq 62.5\ k\Omega \\
        & 12\ \mu{A} \geq \frac{10\ V - 0.5\ V}{R18 + R19}
            \rightarrow R18 \geq 396\ k\Omega
    \end{align*}
    Using 470 kΩ for all of R9, R10, R14, R15, R18 and R19 will simplify the
    BOM and allow for more cherry picking.  R14 and R15 could use as low as
    68 kΩ

    Only the 50\% dividers have been selected with fixed resistors, the rest
    are done with trimmer potentiometers: \\
    \begin{tabular}{|l|ll|}
        \hline
        P4 & 1/4 accurate divider & loads signal output (100kΩ) \\
        P5 & 1/5 accurate divider & loads signal output (100kΩ) \\
        P6 & 1/5 accurate divider & loads \(V_{ref}\) (100kΩ) \\
        \hline
    \end{tabular}
    \\
    The actual value is not critical, but calculations for the TL431 assume
    P6 is 100k and P4 and P5 are assumed to not cause any noticeable load on
    the voltage output. \\
    The PCB is designed for \code{V10-*} footprints for P4, P5 and P6.
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{G to U gain: P2, P3}
    P2 and P3 are combined to tolerate the potentially high power dissipation
    of \(I_{LDR,max}\cdot15\ V = 0.7\ W\).  They still need to tolerate half
    of that each so a couple of larger cermet trimmer potentiometers have been
    chosen.  \code{T910Y-*} or \code{T910W-*} are rated for 0.5 Watts, have
    the screw on an accessible side, and also has the benefit of a
    low \(\pm 100\ ppm/\degree{C}\) temperature coefficient.
    \par
    The total range of resistance needs to include 320Ω to 8kΩ.  The PCB is
    designed for two \code{T910Y-*} in series. Preferrably \code{-5K} (5kΩ)
    but can be larger.
\end{nopbrk}
\begin{nopbrk}
    \subsubsection{U to I gain: P7}
    P7 is used as a 125 ohm resistor. Max power dissipation in the circuit is
    \((20\ mA)^2 \cdot 125\ \Omega = 50\ mW\).  The PCB is designed for a
    \code{V10-*} footprint.  Needs to be \code{V10-250} or larger resistance.
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{Comparator output: R25, P8, P9}
    P8 sets the digital output threshold level. No specific value required but
    should be significantly smaller than P9. The PCB is designed for a
    \code{P10-*} footprint.  10 kΩ is a nice round value.

    P9 sets the digital output's hysteresis. No specific value required but
    should be significantly larger than P8. The PCB is designed for a
    \code{P10-*} footprint.  1 MΩ is a nice round value that is a lot larger
    than 10 kΩ but still not insanely high.

    R25 sets the maximum amount of hysteresis to the digital output and also
    limits the current from \(V_{ref}\) to ground, \(V_{ref}\) to \(V_S\)
    and \(V_S\) to ground that goes through the opamp should P9 be set to zero.
    \\
    47 kΩ gives an insane amount of hysteresis while also more than
    sufficiently limiting the current. Assuming the output from the
    opamp is either 20 volts or zero and that P9 is set to 50\%, it will
    change the trigger level:
    \begin{align*}
        & trig_{norm} = ref / 2 = 50\% \\
        & trig_{high} = \frac{\frac{ref}{5k} + \frac{0}{5k} + \frac{8 ref}{33k}}
                             {\frac{1}{5k}   + \frac{1}{5k} + \frac{1}{33k}}
                      = 87.9\% \\
        & trig_{low} = \frac{\frac{ref}{5k} + \frac{0}{5k} + \frac{0 ref}{33k}}
                            {\frac{1}{5k}   + \frac{1}{5k} + \frac{1}{33k}}
                     = 47.5\% \\
    \end{align*}
    With P8 set to 50\% and P9 set to max hysteresis the actual
    trigger level will be 67.7\% and the hysteresis will be \(\pm 20.2\%\).
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{7815CP circuitry}
    The 7815 datasheet suggests a 100 nF bypass capacitor on output (C5) and a
    330 nF on input (C4), but since there are no 330 nF in E3 a 470 nF
    (or even a 220 nF depending on the layout) can be used intead.

    %It also recommends a protection diode to protect the component from
    %reverse current if the input rail (somehow) falls faster than the output
    %rail.

    %TODO: What does the datasheet recommend? Is a 1N4001 fast enough?

    The 7815 needs a heatsink because it supplies up to 87 mA at a voltage
    drop that can be as high as \(29\ V - 14.25\ V = 14.75\ V\) and up to 
    8 mA quiescent current.
        
    The 15 V rail exists to save three transistors from the very same fate.
    Max power dissipation is 1.52 W.

    Maximum power consumption:
    \begin{align*}
        & V_{S,max} = 29\ V \\
        & V_{min} = 14.25\ V \\
        & I_{q,max} = 8\ mA \\
        & I_{O,max} = I_{analog\_voltage,max} + I_{analog\_current,max}
                      + I_{LDR,max} = (20+20+47)\ mA \\
        & P_{max} = V_{S,max} \cdot I_{q,max}
                    + (V_{S,max} - V_{min}) \cdot I_{O,max}
                  = 1.52\ W
    \end{align*}

    \code{analog\_voltage} can supply more than 20 mA if shorted
    (up to 100), but since the 7815 has over temperature protection
    there will be no permanent damage. \\
\end{nopbrk}
\begin{nopbrk}
    A small heatsink such as the \href
    {https://www.starelec.fi/product\_info.php?cPath=47\_1133\_1140&products\_id=10908}
    {6390B} is required for operating correctly.

    Assuming the 6390B will be used, or any other 17.4 °C/W heatsink:
    \begin{align*}
        & T_{A,max} = 70\ \degree{C} \\
        & T_{J,max} = 125\ \degree{C} \\
        & R_{\theta JC} = 5\ \degree{C}/W \\
        & R_{\theta SA} = 17.4\ \degree{C}/W \\
        & P = 1.52\ W \\
        & P \cdot (R_{\theta JC} + R_{\theta CS} + R_{\theta SA})
          \leq T_{J,max} - T_{A,max} \\
        & \rightarrow
          R_{\theta CS,max} = \frac{T_{J,max} - T_{A,max}}{P}
                              - R_{\theta JC} - R_{\theta SA} \\
        & R_{\theta CS,max} = \frac{55\ K}{1.52\ W} - 22.4\ K/W
                            = 13.7\ K/W \\
        & T_{J-A,max} = P \cdot (R_{\theta JC} + R_{\theta CS} + R_{\theta SA})
                    = 34\ \degree{C} + 1.52\ W \cdot R_{\theta CS}
    \end{align*}
    The thermal interface material needs a conductance of at least 73 mW/K
    (resistance less than 13.7 K/W), which sounds reasonable.

    The die temperature will (at full load) be more than 34 °C hotter than
    ambient, which gets very hot at 70 °C ambient.

    Due to board size limitations, the 6390B has been used on the PCB layout
    instead of the better performing SK104 or SK129.
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{TL431 circuitry}
    U1 (TL431) requires a minimum cathode current of 400 µA to operate
    correctly.  The load on \(V_{ref}\) consists of R5+P1+M1 (7.8 kΩ),
    P6 (100 kΩ), P8 (10 kΩ), R25* (47 kΩ) and U6 (which actually sources
    current).
    \begin{align*}
        I_{min} = I_{K,min}
                  + \frac{2.5\ V}{7.8\ k\Omega}
                  + \frac{2.5\ V}{100\ k\Omega}
                  + \frac{2.5\ V}{10\ k\Omega}
                  + \frac{2.5\ V}{47\ k\Omega}
                = 1.05\ mA \\
    \end{align*}
    \par
    Assuming a 5\% tolerance and 250 mW power limit:
    \begin{align*}
        & I_{headroom} = \frac{(19 - 2.5)\ V}{1.05 R} - I_{min} \\
        & P_{headroom} = 250\ mW - \frac{(29\ V - 2.5\ V)^2}{0.95 R} \\
    \end{align*}
    \begin{tabular}{|r|r|r|}
        \hline
        \textbf{Resistance (kΩ)} &
        \textbf{current headroom (mA)} &
        \textbf{power headroom (mW)} \\
        \hline
        2.2 & 6.09 & \textbf{FAIL} \\
        3.3 & 3.71 & 25 \\
        4.7 & 2.29 & 92 \\
        6.8 & 1.26 & 141 \\
        10.0 & 0.52 & 176 \\
        15.0 & \textbf{FAIL} & 200 \\
        \hline
    \end{tabular}

    4.7 or 6.8 kΩ is a good choice.
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{Miscellaneous}
    C1 is a largeish ceramic or film capacitor to be placed as close to the
    supply input as possible to soak up noise and hopefully any ESD at the
    connector.
    \\
    C2 is a 10 µF electrolytic bulk capacitor to be places wherever it fits
    on the board.

    C12 and C13 provide bypass for digital outputs, each of the outputs
    should also have ground terminals close to respective capacitor. Due to
    the high current these should have a decent amount of capacitance.
    \\
    Each output also has an optional back-EMF diode (D4 and D5).
    Doesn't really matter for a prototype as any inductive load should have
    it on the load side anyway. \update{D4 and D5 should preferrably be
    schottky diodes or very fast diodes. 1N4148 has been chosen which
    hopefully will be adequate.}

    D3 needs to have lowish leakage as the R9/R10 divider is designed for an
    op-amp that (in the worst case) may be unable to sink more than 12 µA.
    A leakage below 120 nA would result in less than 1\% error.
    \\
    % This is not important at all. The leakage isn't going to toggle on and
    % off.
    % Vishay; 1N4149 and 1N4448 from Starelec says <= 25 nA at 20 V
    A 1N4148 has less than 15 nA leakage at the full 6.25 volts.
    0.1\% error.
    %\\
    %% Starelec
    %A J133 (JFET) has at most 1 nA leakage and would be a ever so slightly
    %better option for a presumably imperceptible below 0.01\% error.

    D6 exists to prevent U10 to back feed the 15 V rail through the
    base-collector junction of Q5 if the output is disconnected. Speed is not
    a concern.  Any normal diode will do fine.

    C14 was added experimentally to fix some ringing found in LTspice. 1pF was
    too little, 10 pF did the job.  C14 may or may not be needed in the real
    circuit and the capacitance required may be something else.
    \\
    It must not be too large.  The rise time of the LDR is 20 ms, C14 must not
    cause the sensor to be slower: the time constant of C14 and R9 should be
    at least an order of magnitude lower than 20 ms.
    \(20\ ms / 470\ k\Omega / 10 = 4.2\ nF\)
    \\
    It probably doesn't have any other adverse effects than being a low pass
    filter, so maybe 100 pF to be on the safe side.
\end{nopbrk}

