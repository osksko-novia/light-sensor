\begin{nopbrk}
    \subsection{Partial schematics}
    \subsubsection{Conductance to voltage conversion}
    \begin{wrapfigure}{l}{11cm}
        \includegraphics[width=11cm]{images/G-to-U.png}
    \end{wrapfigure}
    U3 with the help of Q1 will maintain a contant voltage
    $$\left(\frac{1}{range}\right)^\gamma \cdot V_{ref}$$ (where \(range\) is
    1 or 100 and \(V_{ref}\) is 2.5 volts) over the LDR. The current is
    sensed with the combination of P2 and P3, and then sent off to a unity
    gain differential amplifier (U5) with the sensitive low side through a
    buffer (U4).
    \par
    The full scale voltage across P2 + P3 should be 10 volts which is 4 times
    the reference voltage used.  Assuming range is 1 (meaning 1 000 Lux),
    the correct resistance (\(R_{gain}\)) is:
    \begin{align*}
        & R_{1000} = \frac{R_{10}}{100^\gamma} \\
        & R_{gain} = \frac{10\ V}{V_{ref}} \cdot R_{1000}
    \end{align*}
    The actual value varies between
    \begin{align*}
        & R_{gain,min} = 4 \cdot \frac{8\ k\Omega}{100^{1.0}} = 320\ \Omega \\
        & R_{gain,max} = 4 \cdot \frac{20\ k\Omega}{100^{0.5}} = 8 000\ \Omega
    \end{align*}
    \\
\end{nopbrk}
\begin{nopbrk}
    The 15 V rail exists for these two transistors as well as the current
    output to limit the amount of power dissipated in the transistors.
    \par
    The 15 V rail limits the current through the LDR to 1.5 times full scale
    current:
    \begin{align*}
        & I_{LDR,full} = \frac{V_{ref}}{R_{1000,min}}
                       = \frac{2.5\ V}{80\ \Omega}
                       = 32\ mA \\
        & I_{LDR,max} = 1.5 \cdot I_{LDR,full} = 47\ mA
    \end{align*}
    (\(I_{LDR,full}\) will also be used in some later calculations.)
    \\
\end{nopbrk}
\begin{nopbrk}
    The power dissipation in P2 + P3 can be as high as:
    \begin{align*}
        & P = I_{LDR,max} \cdot 15\ V = 0.7\ W
    \end{align*}
    \\
\end{nopbrk}
\begin{nopbrk}
    R6 prevents the op-amp from forcing any significant current through
    Q1 in the even of more than 1.5 times the max illuminance.
    \par
    R11 protects U5 from a shorted output signal. R24 offers limited protection
    for Q2 in the event of a short circuit.
    \par
    R9 and R10 need to be quite large as the voltage over LDR will be very
    low on the 100 000 Lux scale and the LM358 doesn't have a very good
    sinking capability at low output voltages.
    \\
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{\texttt{range\_select}}
    \begin{wrapfigure}{l}{11cm}
        \includegraphics[width=11cm]{images/range-select.png}
    \end{wrapfigure}
    In order to accomodate two different ranges (1 000 Lux and 100 000 Lux)
    the voltage across the LDR can be reduced by a constant factor. The full
    scale current will remain the same.
    \par
    The constant factor is \(\left(\frac{1}{100}\right)^\gamma\) and is set
    by a voltage divider consisting of R5 (6.8 kΩ) on the high side and
    P1 (1 kΩ) with the output at P1's wiper. As \(\gamma\) is known to be
    between 0.5 and 1.0, the constant factor can only be between 1\% and 10\%
    hence R5 is used to eliminate some of the useless range.
    \par
    When the 24 V digital input \code{range\_select} is high it enables a
    MOSFET (M1) and hooks the low side of the voltage divider to ground. And
    when it's low, the divider is left floating for the normal voltage for the
    1 000 Lux range.
    \par
    R2, R3 and R4 reduce the signal voltage to something that can be handled by
    the MOSFET. R4 also serves as a pull down. R2, C11, D1 and D2 offer ESD
    protection. C11 also shunts any noise to ground.
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{Voltage limiter}
    \begin{wrapfigure}{l}{11cm}
        \includegraphics[width=11cm]{images/range-limiter.png}
    \end{wrapfigure}
    The output is guaranteed to be from 0 to 10 volts. It can't be less than
    zero as there it would require the LDR to have negative resistance and also
    because there is no negative rail.
    Nothing however prevents it from exceeding 10 volts and it could approach
    reach 15 volts in sufficiently bright conditions.
    \par
    The comparator U6 (actually just an LM358 op-amp) will sense if the output
    starts exceeding 10 V and force the inverting input of U5 high through
    the diode D3.  P4 is used to reduce the signal to a quarter as there is no
    10 V reference to use.
    \par
    P4 must not be too large due to the positive feedback loop!
    \begin{align*}
        \frac{P4}{P4 + R10} \cdot V_{Supply,max} < 10\ V
    \end{align*}
    Other things preventing U6 from causing a latch up:
    \begin{itemize}
        \item U6 operates with negative feedback to keep the output at 10 V;
              the output will not go high enough to cause a lock-up.
        \item The load is in parallel with P4 and there is a dropout voltage
              in the op-amp.
        \item Even without this divider keeping positive feedback through D3
              below 10 V, U5 can pull it down through zener breakdown in Q2.
    \end{itemize}
    \par
    The output from U6 also goes to R12, Q3, R13 and Q4 to form a 24 V PNP
    open-collector digital output (100 mA), with C12 as a bypass and
    optionally D4 for protection against inductive loads.
    \par
    During simulation extreme ringing was noticed when the input rapidly fell
    from 11 volts to below 1.2.  Slowing down U5 with C14 fixed the problem.
    1 pF was not sufficient but 10 pF is in the simulation.  The time constant
    of C14 and R9 should be kept a magnitude or more lower than the LDR's
    response time to avoid any adverse effects.
    \par
    \update{C14 is 100pF in the final circuit}
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{Voltage to current conversion}
    \begin{wrapfigure}{l}{11cm}
        \includegraphics[width=11cm]{images/U-to-I.png}
    \end{wrapfigure}
    The voltage input goes from 0 volts to \(4 \cdot V_{ref} = 10\ V\)
    and the current output goes from \(\frac{1}{5} \cdot 20\ mA = 4\ mA\)
    to \(\frac{5}{5} \cdot 20\ mA = 20\ mA\), ie. the output is
    \(\frac{1}{5}\) bias and \(\frac{4}{5}\) actual signal.
    \par
    R18 to R21, P7, U10 and U11 and Q5 form a
    %\href{http://novia.oskog97.com/%C3%A5r2/ELA18PT02/uppg2/uppg2.pdf}
    %{known good transconductance amplifier}
    known good transconductance amplifier (*1) with a gain of
    \(\frac{20\ mA}{2.5\ V}\) set by P7 at 125 Ω and support a maximum output
    voltage of 10 V. Full range input is \(V_{ref} = 2.5\ V\)
    \par
    To get the right output from the transconductance amplifier it needs to be
    fed with \(\frac{1}{5} \cdot analog\_voltage + \frac{1}{5} \cdot V_{ref}\).
    P5 and P6 reduce both the signal and \(V_{ref}\) to a fifth and is then
    buffered by U7 and U8 before fed into an adder consisting of R14 to R17
    and U9.
    \par
    D6 prevents U10 to back-feed the 15 V rail through the collector-base
    junction of Q5 if the output is open circuit.
    \par
    *) Transconductance amplifier is taken from a
    \href{http://novia.oskog97.com/\%C3\%A5r2/ELA18PT02/uppg2/uppg2.pdf}
    {previous assignement}.  \update{The circuit is better explained in
    part two.}
\end{nopbrk}

\begin{nopbrk}
    \subsubsection{Comparator}
    \begin{wrapfigure}{l}{11cm}
        \includegraphics[width=11cm]{images/comparator.png}
    \end{wrapfigure}
    P4 is reused to get \(\frac{1}{4}\) of the voltage to compare against.
    P9 should be (user) adjustable as well to allow changing the amount of
    hysteresis.  P8 sets the trigger level. When the analog output is below
    the trigger level the digital output is high, and low (high impedance)
    when it's above.

    R25 prevents short circuit if both P8 and P9 is set to max.  It could
    be as low as 10 kΩ for this purpose.  It also limits how extreme the
    hysteresis can be and 47 kΩ seemed like a nice value for that purpose.
\end{nopbrk}
